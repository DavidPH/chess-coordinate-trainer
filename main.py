import pygame as pg
import random
import sys
import threading

pg.init()

black = (0, 0, 0)
white = (255, 255, 255)
dark_blue = (137, 157, 181)
light_blue = (110, 134, 148)
FONT = pg.font.SysFont('Comic Sans MS', 165, True)

display_width = 600
display_height = 600
game_display = pg.display.set_mode((display_width, display_height))
pg.display.set_caption("Coordinate Trainer")
icon = pg.image.load("icon.ico")
pg.display.set_icon(icon)
game_over = False


class Board:
    def __init__(self):
        self.column = [chr(i) for i in range(ord('a'), ord('h') + 1)]
        self.row = [7, 6, 5, 4, 3, 2, 1, 0]
        self.box_array = [[pg.draw.rect(game_display, white, [0, 0, 0, 0]) for _ in range(8)] for _ in range(8)]
        self.w = display_width / 8
        self.h = display_height / 8

    def draw(self):
        color = [white, light_blue, dark_blue]
        x = 0
        y = 0
        flip = -1

        for i in self.row:
            flip *= -1
            for j in range(8):
                self.box_array[j][i] = pg.draw.rect(game_display, color[flip * -1], [x, y, self.w, self.h])
                pg.draw.rect(game_display, white, [x + 1, y + 1, self.w - 2, self.h - 2], 1)
                flip *= -1
                x += self.w
            x = 0
            y += self.h

    def click(self, row, col):
        loc = pg.mouse.get_pos()
        for i in self.row:
            for j in range(8):
                if self.box_array[i][j].collidepoint(loc):
                    if row == i and col == j:
                        return True
        return False

    def hover(self):
        loc = pg.mouse.get_pos()
        for i in self.row:
            for j in range(8):
                if self.box_array[i][j].collidepoint(loc):
                    return i, j


alpha = 200


def generate_alpha():
    global alpha
    while not game_over:
        if alpha > 0:
            alpha -= 1
            pg.time.delay(5)


def make_text(text, color, alpha):
    text = FONT.render(text, True, color, color)
    text.set_colorkey(color)
    text_rect = text.get_rect(center=(display_width / 2, display_height / 2))
    text.set_alpha(alpha)
    game_display.blit(text, text_rect)


def make_box(board, hx, hy, alpha, color=pg.Color("Green")):
    if hx >= 0:
        s = pg.Surface((board.w, board.h))
        s.set_alpha(alpha)
        s.fill(color)
        game_display.blit(s, board.box_array[hx][hy])


def game_loop():
    global alpha
    global game_over
    game_display.fill(white)
    board = Board()
    board.draw()
    alpha = 200
    rand_coll = random.randint(0, 7)
    rand_row = board.row[random.randint(0, 7)]
    pg.mouse.set_cursor(*pg.cursors.broken_x)
    txt = "{}{}".format(board.column[rand_row], rand_coll + 1)
    generate = threading.Thread(target=generate_alpha)
    generate.start()
    pg.display.set_caption("Coordinate trainer: " + txt)
    color = black
    hx, hy = -1, -1
    while not game_over:
        board.draw()
        make_box(board, hx, hy, alpha)
        make_text(txt, color, alpha)
        x, y = board.hover()
        if pg.mouse.get_focused() > 0:
            make_box(board, x, y, 80, pg.Color("Beige"))
        pg.display.update()
        for event in pg.event.get():
            if event.type == pg.QUIT:
                game_over = True
                sys.exit()
            if event.type == pg.MOUSEBUTTONDOWN:
                if board.click(rand_row, rand_coll):
                    color = black
                    pg.display.update()
                    rand_coll = random.randint(0, 7)
                    rand_row = random.randint(0, 7)
                    txt = "{}{}".format(board.column[rand_row], rand_coll + 1)
                    alpha = 200
                    pg.display.set_caption("Coordinate trainer: {}{}".format(board.column[rand_row], rand_coll + 1))
                    hx, hy = board.hover()
                    pg.display.update()
                else:
                    alpha = 200
                    hx, hy = -1, - 1
                    color = pg.Color("red")


game_loop()
